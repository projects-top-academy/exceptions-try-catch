﻿#include <iostream>
#include <string>
#include <stdexcept>

class WrongNumberException : public std::exception {
private:
    std::string message;

public:
    WrongNumberException(const std::string& msg) : message(msg) {}

    const char* what() const throw() {
        return message.c_str();
    }
};

std::string ConvertNumberToOtherFormat(const std::string& number) {
    std::string result = "";
    for (char c : number) {
        if (std::isdigit(c)) {
            result += c;
        }
    }

    if (result.size() == 11 && result[0] == '7') {
        return "+7 (" + result.substr(1, 3) + ") " + result.substr(4, 3) + "-" + result.substr(7, 2) + "-" + result.substr(9, 2);
    }
    else if (result.size() == 10 && result[0] == '7') {
        return "+7 (" + result.substr(0, 3) + ") " + result.substr(3, 3) + "-" + result.substr(6, 2) + "-" + result.substr(8, 2);
    }
    else {
        throw WrongNumberException("Invalid phone number format");
    }
}

int main() {
    std::string phoneNumber;
    std::cout << "Enter a phone number: ";
    std::cin >> phoneNumber;

    try {
        std::string convertedNumber = ConvertNumberToOtherFormat(phoneNumber);
        std::cout << "Converted phone number: " << convertedNumber << std::endl;
    }
    catch (const WrongNumberException& e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }

    return 0;
}